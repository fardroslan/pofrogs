<?php require '../php/authentication/authetication.php';?>
<!DOCTYPE html>
	<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
    <title>PoFrogs System</title>
    <link href="../css/bootstrap.min.css?v=5" rel="stylesheet">
    <link href="../css/enhanced.css" rel="stylesheet">
    <link href="../css/jquery.qtip.min.css" rel="stylesheet">
    <link href="../css/bootstrap-switch.min.css" rel="stylesheet">
	<link href="../css/bootstrap-datepicker.min.css" rel="stylesheet">
	<link href="../css/bootstrap-select.min.css" rel="stylesheet">
	<link href="../css/BootSideMenu.css" rel="stylesheet">
	<link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css?v=2" rel="stylesheet">
    <link href="../css/custom.css?v=5" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="../font_awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <script src="../js/1.11.1/jquery.min.js"></script>
    <script src="../js/bootbox.min.js"></script>
    <script src="../js/jquery.numeric.min.js"></script>
	<script src="../js/bootstrap-datepicker.min.js"></script>
    <script src="../js/jquery.popupWindow.js"></script>
	<script src="../js/bootstrap-switch.js"></script>
	<script src="../js/BootSideMenu.js"></script>
	<script src="../js/jquery.dragscroll.js"></script>
	<script src="../js/bootstrap-datagrid.js"></script>
	<script src="../js/bootstrap-select.min.js"></script>
	<script src="../js/clipboard.min.js"></script>
	<script type="text/javascript" src="../js/canvasjs.min.js"></script>
    <script type="text/javascript" src="../js/jquery.canvasjs.min.js"></script>
    <script src="../js/bootstrap-notify.min.js"></script>
    <script src="../js/jquery.cookie.js"></script>
    <script src="../js/jquery.number.min.js"></script>
	<script src="../script/frogtype.js?v=10"></script>

    </head>
  <body>
  <div id="wrapper">
  
  		<div id="form_panel" style="min-width:40% !important;">
			<div class="navbar navbar-default" style="border-radius: 0px !important; margin-bottom:0px !important">
			  <div class="container-fluid" id="return_status_btn_panel">
				  <!-- Brand and toggle get grouped for better mobile display -->
				  <a class="navbar-brand" style="font-size:15px !important; cursor:pointer">Form</a>
				  <button id="delete_btn" type="button" class="btn btn-danger status_btn disabled" style="margin:7px 2px; float:right; "><i class="fa fa-trash fa-1x" aria-hidden="true"></i> Remove</button>
				  <button id="reset_btn" type="button" class="btn btn-warning status_btn" style="margin:7px 2px; float:right; "><i class="fa fa-eraser fa-1x" aria-hidden="true"></i> Clear</button>
				  <button id="save_btn" type="button" class="btn btn-primary status_btn" style="margin:7px 2px; float:right; "><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
				  <!-- Collect the nav links, forms, and other content for toggling -->
			  </div><!-- /.container-fluid -->
		  	</div>

			<div class="container" style="width:100% !important; position:absolute; padding:10px 20px !important; overflow-y: auto; height:100%">
				
				<div class="row">
					<form class="" id="report_form" name="report_form">
					
					<input type="hidden" id="reportid" name="reportid" value="" readonly>
					<input type="hidden" id="action" name="action" value="" readonly>
					

					<div class="col-xs-12 col-md-12">
					<h5><i class="fa fa-file-text-o" aria-hidden="true"></i> Frog's Type</h5>
						<div class="row">
						  <div class="col-xs-12 col-md-12">
							<div class="input-group input-group-sm" style="padding-bottom:5px !important">
						      <span class="input-group-addon input-sm" style="line-height: 0 !important; text-align:left">Title</span>
						      <input type="text" class="form-control input-sm" id="input_title" name="input_title" placeholder="Title"> 
						    </div>
						  </div>
						  <div class="col-xs-12 col-md-12">
					    	<div class="input-group input-group-sm" style="padding-bottom:5px !important; width:100%">
					      		<textarea style="height:120px !important; width:100%; resize: none;" id="input_description" name="input_description" type="text" class="input-sm form-control" placeholder="Description"></textarea>
					    	</div>
					  	  </div>
						</div>
					</div>
			
					</form>
				</div>
			</div>
		</div>
  	
  		<!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="font-size:large; padding:10px" href="../main.php"> <img src="../img/logo_main.gif" style="height:30px; display: inline-block;"> &nbsp; <span style="display:inline-block;">PoFrogs</span></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li><a href="#">No message.</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li><a href="#">No notification.</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php if($_COOKIE['PoF_UserDisplayName'] != "") echo $_COOKIE['PoF_UserDisplayName']; else echo ""; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-fw fa-user"></i> <strike>View Profile</strike></a></li>
					  	<?php if($_COOKIE['PoF_UserGroup'] == "PoF_Admin"){ ?>
					  	<li><a href="#"><i class="fa fa-fw fa-users"></i> <strike>User Management</strike></a></li>
					  	<li><a href="../viewlog.php"><i class="fa fa-fw fa-list"></i> View Logs</a></li>
					  	<?php } ?>
					  	<li class="divider"></li>
					 	<li><a id="logoutBtn" style="role="menuitem" tabindex="-1" href="<?php echo $logoutAction ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div id="sys_menus" class="collapse navbar-collapse navbar-ex1-collapse"></div>
            <!-- /.navbar-collapse -->
       </nav>
  
		<div id="page-wrapper" style="height:100%">
            <div class="container-fluid">
	            <!-- Collect the nav links, forms, and other content for toggling -->
	            <div class="row"><div class="col-lg-12" style="background-color:#054D7D">
				  <ul class="nav menu-nav">
				  	  <li><a id="refresh_btn" style="cursor:pointer !important;"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a></li>
				  	  <li><a id="new_btn" style="cursor:pointer !important;"><i class="fa fa-file-text-o" aria-hidden="true"></i> New Type</a></li>
				  </ul>
            	</div></div>
            	
            	 <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="page-header" style="margin: 10px 0 10px; margin-left:30px" /><i class="fa fa-file-text-o" aria-hidden="true"></i> List of Frog's Type</h4>
                    </div>
                </div>
                <!-- /.row -->
                
                <div class="row">
                    <div class="col-lg-12">
			          <div class="panel-heading"><span style="display:inline; float:right">Total record:- <label id="record">0/0</label></span>
					  <ul class="pager">
						<li id="nav_prev" class="disabled" data=""><a id="nav_prev_btn" href="#"><span aria-hidden="true">&larr;</span> Previous</a></li> <!--label id="nav_currentpage">0</label-->
						<select id="nav_currentpage" class="select input-sm" style="padding:0px !important; margin:0px !important; height:24px !important; line-height:24px !important">
							<option value="0">0</option>
						</select> of <label id="nav_totalpage">0</label>
						<li id="nav_next" class="disabled" data=""><a id="nav_next_btn"  href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
					  </ul>
			          </div>
			            <div class="table-responsive double-scroll" style="overflow-x: scroll; min-height:300px !important">
			              <table id="data_load" class="table table-hover table-condensed table-responsive table-striped table-bordered table-fixed-header">
			                <thead class="header">
			                <tr>
			                <th style="text-align:center; min-width:30px; width:5% !important; background-color:#ECECEC !important">No.</th>
			                <th style="text-align:left; min-width:200px; width:25% !important; background-color:#ECECEC !important">Title</th>
			                <th style="text-align:left; min-width:500px; width:70% !important; background-color:#ECECEC !important">Description</th>
			                </tr>
			                </thead>
			                <tbody id="filedata"></tbody>
			              </table>
			        	</div>
                    </div>
                </div>
                
        	</div>
        </div>
        <div class="panel-footer">
	      <div class="container">
	      	<div class="row">
	        	<div class="col-lg-12">
	            </div>
	        </div>
	      </div>
	    </div>
	</div>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/bootstrap.min.js"></script>
</body></html>