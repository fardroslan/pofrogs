<?php
//structure of main submenu
$menu_pond = '
<ul id="menu_pond" class="collapse in">
	<li><a href="report.php"> View Report </a></li>
</ul>
';
//structure of main submenu
$menu_frog = '
<ul id="menu_frog" class="collapse in">
	<li><a href="froginfo.php"> List of Frogs </a></li>
	<li><a href="froggroup.php"> Manage Group</a></li>
    <li><a href="frogtype.php"> Manage Type </a></li>
</ul>
';
//main structure of main menu 
$menu = '
<ul class="nav navbar-nav side-nav">
    <li class="active"><a href="../main.php"><i class="fa fa-fw"></i> Dashboard</a></li>
	<li><a href="javascript:;" data-toggle="collapse" data-target="#menu_pond"><i class="fa fa-folder-open" aria-hidden="true"></i> Pond Info<i class="fa fa-fw fa-caret-down"></i></a>'.$menu_pond.'</li>
    <li><a href="javascript:;" data-toggle="collapse" data-target="#menu_frog"><i class="fa fa-folder-open" aria-hidden="true"></i> Frog Info <i class="fa fa-fw fa-caret-down"></i></a>'.$menu_frog.'</li>
</ul>';

echo $menu;
?>