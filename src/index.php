<?php require_once('Connections/api.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>
<?php
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable PoF_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}
?>
<?php
// *** Validate request to login to this site.
if (!isset($_SESSION)) {
  session_start();
}

$loginFormAction = $_SERVER['PHP_SELF'];
if (isset($_GET['accesscheck'])) {
  $_SESSION['PrevUrl'] = $_GET['accesscheck'];
}
if (isset($_POST['USERNAME'])) {
  $loginUsername=$_POST['USERNAME'];
  $password=$_POST['PASSWORD'];
  $MM_fldUserAuthorization = "";
  $MM_redirectLoginSuccess = "loginsuccess.php";
  $MM_redirectLoginFailed = "loginfail.php";
  $MM_redirecttoReferrer = true;
  mysql_select_db($database_api, $api);
  
  $LoginRS__query=sprintf("SELECT * FROM sys_acc WHERE username = %s AND password = %s AND status = %s",
    GetSQLValueString($loginUsername, "text"), GetSQLValueString(md5($password), "text"), GetSQLValueString("Active", "text")); 
   
  $LoginRS = mysql_query($LoginRS__query, $api) or die(mysql_error());
  $row_LoginRS = mysql_fetch_assoc($LoginRS);
  $loginFoundUser = mysql_num_rows($LoginRS);
  if ($loginFoundUser) {
	  if($row_LoginRS["SESSION_ID"] != $_COOKIE["PHPSESSID"] && $row_LoginRS["SESSION_ID"] != NULL){
			mysql_query("START TRANSACTION");
			$updateSQL = sprintf("UPDATE sys_acc SET sessionid = NULL WHERE id = %s",
						   GetSQLValueString($row_LoginRS['id'], "int"));
			mysql_select_db($database_api, $api);
			$Result1= mysql_query($updateSQL, $api) or die(mysql_error());
			if(!$Result1){
			  mysql_query("ROLLBACK");
			}else{
			  mysql_query("COMMIT");
			  $_SESSION["CurrentSession"] = $_COOKIE["PHPSESSID"];
			}
			header("Location: sessionfail.php");
			exit();
		}else if($row_LoginRS["SESSION_ID"] == $_COOKIE["PHPSESSID"] || $row_LoginRS["SESSION_ID"] == NULL){
			$loginStrGroup = "PoF_".$row_LoginRS['type'];
			if (PHP_VERSION >= 5.1) {session_regenerate_id(true);} else {session_regenerate_id();}
			//declare two session variables and assign them
			$_SESSION['PoF_Username'] = $loginUsername;
			$_SESSION['PoF_UserGroup'] = $loginStrGroup;	  
			$_SESSION['PoF_UserID'] = $row_LoginRS['id']; 
			$_SESSION['PoF_UserLastLogin'] = $row_LoginRS['lastlogin']; 
			$_SESSION['PoF_UserDisplayName'] = $row_LoginRS['displayname'];    
			
			setcookie("PoF_Username", $loginUsername, time() + 172800, "/"); // 172800 = 2 day
			setcookie("PoF_UserGroup", $loginStrGroup, time() + 172800, "/"); // 172800 = 2 day
			setcookie("PoF_UserID", $row_LoginRS['id'], time() + 172800, "/"); // 172800 = 2 day
			setcookie("PoF_UserLastLogin", $row_LoginRS['lastlogin'], time() + 172800, "/"); // 172800 = 2 day
			setcookie("PoF_UserDisplayName", $row_LoginRS['displayname'], time() + 172800, "/"); // 172800 = 2 day
			
			if (isset($_SESSION['PrevUrl']) && true) {
			  $MM_redirectLoginSuccess = $_SESSION['PrevUrl'];	
			}
			header("Location: " . $MM_redirectLoginSuccess );
		}
  }
  else {
    header("Location: ". $MM_redirectLoginFailed );
  }
}
mysql_close($api);
?>
<!DOCTYPE html>
	<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <title>PoFrogs System</title>
    <link href="css/bootstrap.min.css?v=5" rel="stylesheet">
    <link href="css/enhanced.css" rel="stylesheet">
    <link href="css/jquery.qtip.min.css" rel="stylesheet">
    <link href="css/style.css?v=6" rel="stylesheet">
    <link href="css/custom.css?v=1" rel="stylesheet">
    <script src="js/1.11.1/jquery.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.popupWindow.js"></script>
	<script src="script/api.js?v=2"></script>
    <script src="js/jquery.tree.js"></script>
    <script src="js/jquery.qtip.min.js"></script>
    <script src="js/imagesloaded.pkg.min.js"></script>
    <link rel="stylesheet" type="text/css" href="cleditor/jquery.cleditor.css?v=1" />
	<script type="text/javascript" src="cleditor/jquery.cleditor.min.js?v=1"></script>
    </head>
  <body>
  	<div class="panel-header" style="padding-top: 10px;">
	<div class="container" style="width:100% !important;height: 170px;margin: 0px;text-align: center;">
            <h3 class="xprint" style="margin:0px 0px -5px !important;color: #888; padding: 15px 10px !important;"><img style="padding:10px" src="img/logo_main.gif" style=""> <br> <span style="vertical-align: middle; font-size: 20px;color: #000;font-weight: 600;display: block;">PoFrogs Management System</span></h3>
        </div>
   	  
    </div>
    <div class="container" style="width:100% !important;padding-bottom: 50px; text-align: center;">
      <div class="starter-template" style="text-align: center">
        <p class="lead"></p>
        <form ACTION="<?php echo $loginFormAction; ?>" METHOD="POST" id="login_form">
        <table class="table table-condensed table-responsive" style="margin-right: auto; margin-left: auto; max-width: 300px !important;">
        <tr style="border:none !important" >
            <td align="center" style="width:100%; text-align: center"><input type="text" class="form-control input" id="USERNAME" name="USERNAME" placeholder="Enter Username" style="max-width:300px !important"></td>
        </tr>
        <tr style="border:none !important">
            <td align="center" style="width:100%; text-align: center !important"><input type="password" class="form-control input" id="PASSWORD" name="PASSWORD" placeholder="Enter Password" style="max-width:300px !important"></td>
        </tr>
        <tr style="border:none !important">
            <td align="center" width:100%; text-align: center"><input style="width:100%; max-width:300px !important" type="submit" class="btn btn-gemfive btn" value="Login"> <!--a style="padding-left:10px; cursor:pointer">Forgot Password?</a--></td>
        </tr>
        </table>
        </form>
      </div>
    </div><!-- /.container -->
    
    <div id="loading" style="z-index: 9999999; opacity: 0.3; position: fixed; top: 0px; right: 0px; bottom: 0px; left: 0px; display: none; background-color: rgb(0, 0, 0);">
		<ul class="bokeh"><li></li><li></li><li></li></ul>
	</div>
    
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
</body></html>