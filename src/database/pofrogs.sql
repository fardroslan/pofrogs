-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: pofrogs
-- ------------------------------------------------------
-- Server version	5.7.17-11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `frog_group`
--

DROP TABLE IF EXISTS `frog_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frog_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `sys_status` varchar(45) DEFAULT 'Active',
  `sys_status_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frog_group`
--

LOCK TABLES `frog_group` WRITE;
/*!40000 ALTER TABLE `frog_group` DISABLE KEYS */;
INSERT INTO `frog_group` VALUES (1,'Group A','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ','Active','2017-03-19 21:52:30'),(2,'Group B','Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.','Active','2017-03-19 21:52:30'),(3,'Group C','In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.','Active','2017-03-19 21:52:30'),(4,'Group D','Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.','Active','2017-03-19 21:52:30'),(5,'Group E','Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.','Active','2017-03-19 21:52:30');
/*!40000 ALTER TABLE `frog_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frog_info`
--

DROP TABLE IF EXISTS `frog_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frog_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `height` varchar(15) DEFAULT NULL,
  `weight` varchar(15) DEFAULT NULL,
  `color` varchar(15) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `date_death` date DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `tag_code` varchar(15) DEFAULT NULL,
  `typeid` int(11) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `createdby` bigint(20) DEFAULT NULL,
  `sys_status` varchar(15) DEFAULT 'Active',
  `sys_status_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_frog_type_idx` (`typeid`),
  KEY `fk_frog_group_idx` (`groupid`),
  KEY `fk_sys_acc_froginfo_idx` (`createdby`),
  CONSTRAINT `fk_frog_group` FOREIGN KEY (`groupid`) REFERENCES `frog_group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_frog_type` FOREIGN KEY (`typeid`) REFERENCES `frog_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_sys_acc_froginfo` FOREIGN KEY (`createdby`) REFERENCES `sys_acc` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frog_info`
--

LOCK TABLES `frog_info` WRITE;
/*!40000 ALTER TABLE `frog_info` DISABLE KEYS */;
INSERT INTO `frog_info` VALUES (1,'Johny the frog','Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc','5.4','19','Green, Yellow','2017-03-06',NULL,'Male','0010',1,2,1,'Active','2017-03-19 21:55:59'),(2,'Merry the frog','Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt.','3.5','14.5','Brown, Grey','2017-03-06',NULL,'Female','1101',2,1,2,'Active','2017-03-19 21:55:59'),(3,'Usher the frog','Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc','5.9','21.6','Red, Green','2017-03-01',NULL,'Male','0011',5,4,1,'Active','2017-03-19 21:59:04');
/*!40000 ALTER TABLE `frog_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `frog_type`
--

DROP TABLE IF EXISTS `frog_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frog_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `sys_status` varchar(45) DEFAULT 'Active',
  `sys_status_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `frog_type`
--

LOCK TABLES `frog_type` WRITE;
/*!40000 ALTER TABLE `frog_type` DISABLE KEYS */;
INSERT INTO `frog_type` VALUES (1,'True toad','A true toad is any member of the family Bufonidae, in the order Anura. This is the only family of anurans in which all members are known as \"toads\", although some may be called frogs.','Active','2017-03-19 21:51:02'),(2,'Poison dart frog','Poison dart frog is the common name of a group of frogs in the family Dendrobatidae which are native to tropical Central and South America. These species are diurnal and often have brightly colored bodies.','Active','2017-03-19 21:51:02'),(3,'Hylidae','The Hylidae are a wide-ranging family of frogs commonly referred to as \"tree frogs and their allies\". However, the hylids include a diversity of frog species, many of which do not live in trees, but are terrestrial or semiaquatic.','Active','2017-03-19 21:51:02'),(4,'Glass frog','The glass frogs are frogs of the amphibian family Centrolenidae. While the general background coloration of most glass frogs is primarily lime green, the abdominal skin of some members of this family is translucent.','Active','2017-03-19 21:51:02'),(5,'South American horned frogs','Ceratophrys is a genus of frogs in the family Ceratophryidae. They are also known as South American horned frogs as well as Pacman frogs due to their characteristically round shape and large mouth','Active','2017-03-19 21:51:02'),(6,'True frog','The true frogs, family Ranidae, have the widest distribution of any frog family. They are abundant throughout most of the world, occurring on all continents except Antarctica.','Active','2017-03-19 21:51:02'),(7,'Xenopus','Xenopus is a genus of highly aquatic frogs native to sub-Saharan Africa. Twenty species are currently described in the Xenopus genus.','Active','2017-03-19 21:51:02'),(8,'Fire-bellied toad','The fire-bellied toads or firebelly toads are a group of eight species of small frogs belonging to the genus Bombina.','Active','2017-03-19 21:51:02'),(9,'Mantella','Mantella is a genus of frogs in the family Mantellidae, endemic to Madagascar. These are small frogs typically reaching 2–3 centimetres, with iridescent colors with combinations of black, blue, orange','Active','2017-03-19 21:51:02'),(10,'Tailed frog','The tailed frogs are two species of frogs in the genus Ascaphus, the only taxon in the family Ascaphidae. The \"tail\" in the name is actually an extension of the male cloaca','Active','2017-03-19 21:51:02');
/*!40000 ALTER TABLE `frog_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_acc`
--

DROP TABLE IF EXISTS `sys_acc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_acc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `type` varchar(25) NOT NULL DEFAULT 'Normal',
  `displayname` varchar(100) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `status` varchar(50) DEFAULT 'Active',
  `sys_status` varchar(25) NOT NULL DEFAULT 'Active',
  `sessionid` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_acc`
--

LOCK TABLES `sys_acc` WRITE;
/*!40000 ALTER TABLE `sys_acc` DISABLE KEYS */;
INSERT INTO `sys_acc` VALUES (1,'farhad','482c811da5d5b4bc6d497ffa98491e38','Admin','Farhad','2017-03-20 21:50:30','Active','Active','on5bdm6oj70jdr1rv8kojdc2c4'),(2,'wani','482c811da5d5b4bc6d497ffa98491e38','Admin','Wani','2017-03-19 19:25:57','Active','Active',NULL);
/*!40000 ALTER TABLE `sys_acc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_log`
--

DROP TABLE IF EXISTS `sys_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `message` text CHARACTER SET utf8,
  `datetime` datetime DEFAULT NULL,
  `logby` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sys_log_user_acc_idx` (`id`,`logby`),
  KEY `fk_sys_log_user_acc_idx1` (`logby`),
  CONSTRAINT `fk_sys_log_user_acc` FOREIGN KEY (`logby`) REFERENCES `sys_acc` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=ucs2;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_log`
--

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;
INSERT INTO `sys_log` VALUES (24,'Login','[127.0.0.1] Login Successful.','2017-03-19 19:25:57',1),(25,'Login','[127.0.0.1] Login Successful.','2017-03-20 06:22:07',1),(26,'Report: Update','Report-1 has been updated.','2017-03-20 11:24:21',1),(27,'Report: Create','Report-3 has been created.','2017-03-20 11:24:58',1),(28,'Report: Update','Report-3 has been updated.','2017-03-20 11:25:24',1),(29,'Report: Update','Report-3 has been updated.','2017-03-20 11:26:26',1),(30,'Report: Update','Report-3 has been updated.','2017-03-20 11:26:36',1),(31,'Report: Create','Report-4 has been created.','2017-03-20 11:27:16',1),(32,'Report: Remove','Report-4 has been removed.','2017-03-20 11:35:40',1),(33,'FrogGroup: Update','Group-1 has been updated.','2017-03-20 11:50:33',1),(34,'FrogGroup: Update','Group-3 has been updated.','2017-03-20 11:50:47',1),(35,'FrogGroup: Create','Group-6 has been created.','2017-03-20 11:50:54',1),(36,'FrogGroup: Remove','Group-6 has been removed.','2017-03-20 11:50:59',1),(37,'FrogType: Update','Type-8 has been updated.','2017-03-20 11:53:20',1),(38,'FrogType: Create','Type-11 has been created.','2017-03-20 11:53:26',1),(39,'FrogType: Remove','Type-11 has been removed.','2017-03-20 11:53:31',1),(40,'FrogType: Create','Type-12 has been created.','2017-03-20 11:53:54',1),(41,'FrogType: Remove','Type-12 has been removed.','2017-03-20 11:54:02',1),(42,'FrogType: Create','Type-13 has been created.','2017-03-20 11:55:49',1),(43,'FrogType: Remove','Type-13 has been removed.','2017-03-20 11:55:56',1),(44,'FrogType: Create','Type-14 has been created.','2017-03-20 11:56:24',1),(45,'FrogType: Remove','Type-14 has been removed.','2017-03-20 11:57:10',1),(46,'FrogType: Create','Type-15 has been created.','2017-03-20 11:57:23',1),(47,'FrogType: Remove','Type-15 has been removed.','2017-03-20 11:57:29',1),(48,'Report: Update','Report-1 has been updated.','2017-03-20 11:58:03',1),(49,'Report: Update','Report-3 has been updated.','2017-03-20 15:03:09',1),(50,'FrogInfo: Create','Frog-4 has been created.','2017-03-20 15:06:36',1),(51,'FrogInfo: Remove','Frog-4 has been removed.','2017-03-20 15:06:47',1),(52,'FrogInfo: Create','Frog-5 has been created.','2017-03-20 15:10:54',1),(53,'FrogInfo: Remove','Frog-5 has been removed.','2017-03-20 15:11:00',1),(54,'FrogInfo: Update','Frog-3 has been updated.','2017-03-20 15:12:00',1),(55,'Report: Update','Report-1 has been updated.','2017-03-20 15:15:42',1),(56,'Report: Remove','Report- has been removed.','2017-03-20 15:16:01',1),(57,'Login','[127.0.0.1] Login Successful.','2017-03-20 15:21:29',1),(58,'FrogInfo: Create','Frog-6 has been created.','2017-03-20 17:11:35',1),(59,'Login','[127.0.0.1] Login Successful.','2017-03-20 17:42:02',1),(60,'Login','[127.0.0.1] Login Successful.','2017-03-20 18:48:37',1),(61,'FrogInfo: Remove','Frog-6 has been removed.','2017-03-20 18:49:19',1),(62,'Login','[127.0.0.1] Login Successful.','2017-03-20 21:50:30',1);
/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_report`
--

DROP TABLE IF EXISTS `sys_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `no_frog_birth` int(11) DEFAULT '0',
  `no_frog_death` int(11) DEFAULT '0',
  `no_frog_mating` int(11) DEFAULT '0',
  `no_frog_description` varchar(250) DEFAULT NULL,
  `pond_condition` varchar(250) DEFAULT NULL,
  `pond_condition_description` varchar(250) DEFAULT NULL,
  `report_date` date DEFAULT NULL,
  `sys_status` varchar(45) DEFAULT 'Active',
  `sys_status_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `reportby` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sys_acc_report_idx` (`reportby`),
  CONSTRAINT `fk_sys_acc_report` FOREIGN KEY (`reportby`) REFERENCES `sys_acc` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_report`
--

LOCK TABLES `sys_report` WRITE;
/*!40000 ALTER TABLE `sys_report` DISABLE KEYS */;
INSERT INTO `sys_report` VALUES (1,'Daily Report 170318','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ',10,2,4,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ','Good','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ','2017-03-18','Active','2017-03-19 21:46:39',1),(2,'Daily Report 170319','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ',4,0,2,'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ','Average','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ','2017-03-18','Active','2017-03-19 21:47:22',2),(3,'Daily Report 170320','Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.',0,0,0,'Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.','Good','Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.','2017-03-20','Active','2017-03-20 11:24:58',1);
/*!40000 ALTER TABLE `sys_report` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-20 22:08:48
