<?php
require_once ('../../Connections/api.php');
if (!isset($_SESSION)) {
    $session_lifetime = 3600 * 24; // 1 days
    session_set_cookie_params ($session_lifetime);
    session_start();
}
if (!function_exists("GetSQLValueString")) {
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        }

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}

//get ip address of user
function getRealIpAddr(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
$ipaddress = getRealIpAddr();

//function to update log table
function update_log($log_action, $log_message){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $logSQL = sprintf("INSERT INTO sys_log (action, message, datetime, logby) VALUES (%s, %s, now(), %s)",
                GetSQLValueString($log_action, "text"),
                GetSQLValueString($log_message, "text"),
                GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
        mysql_select_db($database_api, $api);
        $logResult = mysql_query($logSQL, $api) or die(mysql_error());

        if(!$logResult) mysql_query("ROLLBACK");
        else mysql_query("COMMIT");
    }return false;
}

//function to create record into frog_group table
function action_create($title, $description){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        $entityid = '';
        mysql_query("START TRANSACTION");
        $insertSQL = sprintf("INSERT INTO frog_group (title, description)
    			VALUES (%s, %s)",
                GetSQLValueString($title, "text"),
                GetSQLValueString($description, "text"),
                GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($insertSQL, $api) or die(mysql_error());
        $GLOBALS['id'] = mysql_insert_id();

        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{
            mysql_query("COMMIT");
            update_log("FrogGroup: Create", "[".$GLOBALS['ipaddress']."] Group-".$GLOBALS['id']." has been created.");
            return true;
        }
    }return false;
}

//function to update record in frog_group table
function action_update($id, $title, $description){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $updateSQL = sprintf("UPDATE frog_group SET title = %s, description = %s WHERE id = %s",
                GetSQLValueString($title, "text"),
                GetSQLValueString($description, "text"),
                GetSQLValueString($id, "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($updateSQL, $api) or die(mysql_error());

        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{
            mysql_query("COMMIT");
            update_log("FrogGroup: Update", "[".$GLOBALS['ipaddress']."] Group-".$id." has been updated.");
            return true;
        }
    }return false;
}

//function to delete record in frog_group table
function action_delete($id){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $deleteSQL = sprintf("DELETE FROM frog_group WHERE id = %s",GetSQLValueString($id, "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($deleteSQL, $api) or die(mysql_error());

        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{
            mysql_query("COMMIT");
            update_log("FrogGroup: Remove", "[".$GLOBALS['ipaddress']."] Group-".$id." has been removed.");
            return true;
        }
    }return false;
}

$return = array();
$json_status = "";
$json_message = "";

$action         = (isset($_POST['action']) ? $_POST['action'] : null);
$id             = (isset($_POST['reportid']) ? $_POST['reportid'] : null);
$title          = (isset($_POST['input_title']) ? $_POST['input_title'] : null);
$description    = (isset($_POST['input_description']) ? $_POST['input_description'] : null);

if($action != ""){
    if(strtolower($action) == "create") $result = action_create($title, $description);
    else if(strtolower($action) == "update") $result = action_update($id, $title, $description);
    else if(strtolower($action) == "delete") $result = action_delete($id);

    if($result){ //return if action success
        $json_status = "success";
        $json_message =  "Record has been ".$action."d successfully.";
    }else{ //return if action failed
        $json_status = "fail";
        $json_message = "Record failed to ".$action.".";
    }

    $return[] = array(
        'status'    => $json_status,
        'message'   => $json_message,
    );
    echo json_encode($return);
}

mysql_close($api);
?>