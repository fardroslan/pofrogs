<?php
require_once ('../../Connections/api.php');
if (!isset($_SESSION)) {
    $session_lifetime = 3600 * 24; // 1 days
    session_set_cookie_params ($session_lifetime);
    session_start();
}

$data_records = array();
$return = array();

//read total of record from frog_type table
mysql_select_db($database_api, $api);
$data = mysql_query("SELECT a.* FROM frog_type a ORDER BY a.title ASC", $api) or die(mysql_error());
$row_data = mysql_fetch_assoc($data);
$totalRows_data = mysql_num_rows($data);

if($totalRows_data > 0) {
    do {
        $data_records[] = array(
            'id'                => $row_data['id'],
            'title'             => $row_data['title'],
            'description'       => $row_data['description'],
            'sys_status'        => $row_data['sys_status'],
            'sys_status_date'   => $row_data['sys_status_date']
        );
         
    } while ($row_data = mysql_fetch_assoc($data));

    $return[] = array(
            'data'         => $data_records,
            'page'         => $pages,
            'currentpage'  => $current_page,
            'totalpage'    => $totalRows_data_total
    );
    echo json_encode($return);
}

mysql_close($api);
?>