<?php
require_once('connection.php');
if (!isset($_SESSION)) {
	$session_lifetime = 3600 * 24; // 1 days
	session_set_cookie_params ($session_lifetime);
	session_start();
}

//set per page for navigation
$per_page = 250;

$page = (isset($_GET['page']) ? $_GET['page'] : null);
$sort = (isset($_GET['sort']) ? $_GET['sort'] : null);

$data_records = array();
$return = array();

$sort = trim($sort);

//read total of record
mysql_select_db($database_api, $api);
$logs_total = mysql_query("SELECT p.*, q.* FROM sys_log q INNER JOIN sys_acc p ON q.logby = p.id ORDER BY q.datetime $sort", $api) or die(mysql_error());
$row_logs_total = mysql_fetch_assoc($logs_total);
$totalRows_logs_total = mysql_num_rows($logs_total);

$pages = ceil($totalRows_logs_total/$per_page);

$current_page = 0;

if($page != ""){
	$start = ($page)*$per_page;
	$current_page = $page;
}else{
	$start = ($current_page)*$per_page;
}

//read total of record based on number per page
mysql_select_db($database_api, $api);
$logs = mysql_query("SELECT p.*, q.* FROM sys_log q INNER JOIN sys_acc p ON q.logby = p.id ORDER BY q.datetime $sort LIMIT $start, $per_page", $api) or die(mysql_error());
$row_logs = mysql_fetch_assoc($logs);
$totalRows_logs = mysql_num_rows($logs);

if($totalRows_logs > 0) {
    do {
        $dateLastLogin = "-";
        if($row_logs['datetime'] != "") $dateLastLogin = date("d-m-Y H:i:s", strtotime($row_logs['datetime']));	        	
        $data_records[] = array(
            'id'            => $row_logs['id'],
            'action'        => $row_logs['action'],
            'message'       => $row_logs['message'],
            'displayname'   => $row_logs['displayname'],
            'datelastlogin' => $dateLastLogin
        );
        	
    } while ($row_logs = mysql_fetch_assoc($logs));

    $return[] = array(
            'data'         => $data_records,
            'page'         => $pages,
            'currentpage'  => $current_page,
            'totalpage'    => $totalRows_logs_total
    );
    echo json_encode($return);
}

mysql_close($api);
?>