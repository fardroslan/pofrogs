<?php require_once('Connections/api.php'); ?>
<?php
ini_set("session.gc_maxlifetime", "86400");
//initialize the session
if (!isset($_SESSION)) {
  $session_lifetime = 3600 * 24; // 1 days
  session_set_cookie_params ($session_lifetime);
  session_start();
}

$_SESSION['PoF_CurentPage'] = 'main.php';
setcookie("PoF_CurentPage", 'main.php', time() + 86400, "/"); // 86400 = 1 day

// ** Logout the current user. **
$logoutAction = $_SERVER['PHP_SELF']."?doLogout=true";
if ((isset($_SERVER['QUERY_STRING'])) && ($_SERVER['QUERY_STRING'] != "")){
  $logoutAction .="&". htmlentities($_SERVER['QUERY_STRING']);
}

?>
<?php
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";

// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable PoF_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "accessfail.php";
if (!((isset($_COOKIE['PoF_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_COOKIE['PoF_Username'], $_COOKIE['PoF_UserGroup'])))) {   
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php 
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

if ((isset($_GET['doLogout'])) &&($_GET['doLogout']=="true")){
  //to fully log out a visitor we need to clear the session varialbles
  	mysql_query("START TRANSACTION");
	$updateSQL = sprintf("UPDATE sys_acc SET sessionid = NULL WHERE id =%s",
				   GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
	mysql_select_db($database_api, $api);
	$Result1= mysql_query($updateSQL, $api) or die(mysql_error());
	if(!$Result1){
	  mysql_query("ROLLBACK");
	}else{
	  mysql_query("COMMIT");
	}
  $_SESSION['PoF_Username'] = NULL;
  $_SESSION['PoF_UserGroup'] = NULL;
  $_SESSION['PrevUrl'] = NULL;
  unset($_SESSION['PoF_Username']);
  unset($_SESSION['PoF_UserGroup']);
  unset($_SESSION['PrevUrl']);
  
  $_COOKIE['PoF_Username'] = NULL; setcookie("PoF_Username", '', time() - 86400, "/");
  $_COOKIE['PoF_UserGroup'] = NULL; setcookie("PoF_UserGroup", '', time() - 86400, "/");
  $_COOKIE['PrevUrl'] = NULL; setcookie("PrevUrl", '', time() - 86400, "/");
  unset($_COOKIE['PoF_Username']);
  unset($_COOKIE['PoF_UserGroup']);
  unset($_COOKIE['PrevUrl']);
	
  $logoutGoTo = "index.php";
  if ($logoutGoTo) {
    header("Location: $logoutGoTo");
    exit;
  }
}


mysql_select_db($database_api, $api);
$query_session = sprintf("SELECT sessionid FROM sys_acc WHERE id = %s",
    GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
$session = mysql_query($query_session, $api) or die(mysql_error());
$row_session = mysql_fetch_assoc($session);
$totalRows_session = mysql_num_rows($session);
if($row_session["sessionid"] != $_COOKIE["PHPSESSID"] && $row_session["sessionid"] != NULL){
	echo $_COOKIE["PoF_UserID"]." || ".$_COOKIE["PHPSESSID"];
	mysql_query("START TRANSACTION");
	$updateSQL = sprintf("UPDATE sys_acc SET sessionid = NULL WHERE id = %s",
				   GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
	mysql_select_db($database_api, $api);
	$Result1= mysql_query($updateSQL, $api) or die(mysql_error());
	if(!$Result1){
	  mysql_query("ROLLBACK");
	}else{
	  mysql_query("COMMIT");
	}
	$_SESSION['PoF_Username'] = NULL;
	$_SESSION['PoF_UserGroup'] = NULL;
	$_SESSION['PoF_UserID'] = NULL;
	$_SESSION['PrevUrl'] = NULL;
	unset($_SESSION['PoF_Username']);
	unset($_SESSION['PoF_UserGroup']);
	unset($_SESSION['PoF_UserID']);
	unset($_SESSION['PrevUrl']);
	$_COOKIE['PoF_Username'] = NULL; setcookie("PoF_Username", '', time() - 86400, "/");
	$_COOKIE['PoF_UserGroup'] = NULL; setcookie("PoF_UserGroup", '', time() - 86400, "/");
	$_COOKIE['PoF_UserID'] = NULL; setcookie("PoF_UserID", '', time() - 86400, "/");
	$_COOKIE['PrevUrl'] = NULL; setcookie("PrevUrl", '', time() - 86400, "/");
	unset($_COOKIE['PoF_Username']);
	unset($_COOKIE['PoF_UserGroup']);
	unset($_COOKIE['PoF_UserID']);
	unset($_COOKIE['PrevUrl']);
	$pageURL = "sessionfail.php";
	header("Location:".$pageURL);
	exit();
}
mysql_close($api);
?>