<?php
require_once ('../../Connections/api.php');
if (!isset($_SESSION)) {
	$session_lifetime = 3600 * 24; // 1 days
	session_set_cookie_params ($session_lifetime);
	session_start();
}

$typeChart = (isset($_GET['chart']) ? $_GET['chart'] : null);
$dataChart = '';

//filter type of chart to return to front end
if(strtolower($typeChart) == 'bygroup'){
    $color = "#5BAD1B";
    $chart = "pie";
    $sql = "SELECT b.title AS title, COUNT(*) AS total FROM frog_info a LEFT JOIN frog_group b ON a.groupid = b.id WHERE a.date_death IS NULL GROUP BY a.groupid";
}else if(strtolower($typeChart) == 'bytype'){
    $color = "#3399FF";
    $chart = "series";
    $sql = "SELECT b.title AS title, COUNT(*) AS total FROM frog_info a LEFT JOIN frog_type b ON a.typeid = b.id WHERE a.date_death IS NULL GROUP BY a.typeid";
}else if(strtolower($typeChart) == 'bygender'){
    $color = "#5BAD1B";
    $chart = "series";
    $sql = "SELECT gender as title, COUNT(*) AS total FROM frog_info WHERE date_death IS NULL GROUP BY gender";
}

//read query based on filter chart to show.
mysql_select_db($database_api, $api);
$query_list = mysql_query($sql." ".trim($filter_statement), $api) or die(mysql_error());
$row_list = mysql_fetch_assoc($query_list);
$totalRows_list = mysql_num_rows($query_list);

if($totalRows_list > 0){	
    $count = 0;
    $chart2show = "";
    do{
        $dataChart = $dataChart.'{"title": "'.$row_list['title'].'","total": '.$row_list['total'].'},';
        $count = (int)$count + (int)$row_list['total'];
    }while($row_list = mysql_fetch_assoc($query_list));
	
    if($chart == "pie"){ //pie chart
        $chart2show = '
        <!-- Styles -->
    	<style>
    	#chartdiv_'.$typeChart.' {
    		width		: 100%;
    		height		: 390px;
    		font-size	: 11px;
    	}
    	</style>
    	<!-- Chart code -->
        <script>
        var chart = AmCharts.makeChart( "chartdiv_'.$typeChart.'", {
          "type": "pie",
          "theme": "none",
          "dataProvider": [ '.$dataChart.' ],
          "valueField": "total",
          "titleField": "title",
           "balloon":{
           "fixedPosition":true
          },
          "export": {
            "enabled": true
          }
        } );
        </script>
        
        <!-- HTML -->
    	<div id="chartdiv_'.$typeChart.'"></div>';
        
    }else if($chart == "series"){ //series chart
            
        $chart2show = '
        <!-- Styles -->
    	<style>
    	#chartdiv_'.$typeChart.' {
    		width		: 100%;
    		height		: 150px;
    		font-size	: 11px;
    	}
    	</style>
        
    	<!-- Chart code -->
    	<script>
    	var chart = AmCharts.makeChart( "chartdiv_'.$typeChart.'", {
    	  "type": "serial",
    	  "theme": "none",
    	  "dataProvider": [ '.$dataChart.' ],
    	  "valueAxes": [ {
    	    "gridColor": "#FFFFFF",
    	    "gridAlpha": 0.2,
    	    "dashLength": 0
    	  } ],
    	  "gridAboveGraphs": true,
    	  "startDuration": 1,
    	  "graphs": [ {
    	    "balloonText": "[[category]]: <b>[[value]]</b>",
    	    "fillAlphas": 1,
    	    "lineAlpha": 0.2,
    	    "type": "column",
    	    "valueField": "total",
    	    "fillColors": "'.$color.'"
    	  } ],
    	  "chartCursor": {
    	    "categoryBalloonEnabled": false,
    	    "cursorAlpha": 0,
    	    "zoomable": false
    	  },
    	  "categoryField": "title",
    	  "categoryAxis": {
    	    "gridPosition": "start",
    	    "gridAlpha": 0,
    	    "tickPosition": "start",
    	    "tickLength": 20
    	  },
    	  "export": {
    	    "enabled": true
    	  }
        
    	} );
    	</script>
        
    	<!-- HTML -->
    	<div id="chartdiv_'.$typeChart.'"></div>';
    }
    
    //return to front end
    echo $chart2show;
}
	
mysql_close($api);
?>