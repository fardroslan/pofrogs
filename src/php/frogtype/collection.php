<?php
require_once ('../../Connections/api.php');
if (!isset($_SESSION)) {
    $session_lifetime = 3600 * 24; // 1 days
    session_set_cookie_params ($session_lifetime);
    session_start();
}

//set singleton id:- request to read single record based on id
$singleton = (isset($_GET['id']) ? $_GET['id'] : null);

$data_records = array();
$return = array();

$per_page = 100; //limit page navigation
$pages = '';
$current_page = '';
$totalRows_data_total = '';

if($singleton == ""){   
    //read total of record from frog_type table
    mysql_select_db($database_api, $api);
    $data_total = mysql_query("SELECT a.* FROM frog_type a ORDER BY a.title ASC", $api) or die(mysql_error());
    $row_data_total = mysql_fetch_assoc($data_total);
    $totalRows_data_total = mysql_num_rows($data_total);
    
    $pages = ceil($totalRows_data_total/$per_page);
    
    $current_page = 0;
    
    if($page != ""){
        $start = ($page)*$per_page;
        $current_page = $page;
    }else{
        $start = ($current_page)*$per_page;
    }
    //read total of record based on number per page from frog_type table
    mysql_select_db($database_api, $api);
    $data = mysql_query("SELECT a.* FROM frog_type a ORDER BY a.title ASC LIMIT $start, $per_page", $api) or die(mysql_error());
    $row_data = mysql_fetch_assoc($data);
    $totalRows_data = mysql_num_rows($data);
}else{
    //read total of record based on number per page from frog_type table **singleton**
    mysql_select_db($database_api, $api);
    $data = mysql_query("SELECT a.* FROM frog_type a WHERE a.id = '$singleton'", $api) or die(mysql_error());
    $row_data = mysql_fetch_assoc($data);
    $totalRows_data = mysql_num_rows($data);
}

if($totalRows_data > 0) {
    do {
        $data_records[] = array(
            'id'                => $row_data['id'],
            'title'             => $row_data['title'],
            'description'       => $row_data['description'],
            'sys_status'        => $row_data['sys_status'],
            'sys_status_date'   => $row_data['sys_status_date']
        );
         
    } while ($row_data = mysql_fetch_assoc($data));

    $return[] = array(
            'data'         => $data_records,
            'page'         => $pages,
            'currentpage'  => $current_page,
            'totalpage'    => $totalRows_data_total
    );
    echo json_encode($return);
}

mysql_close($api);
?>