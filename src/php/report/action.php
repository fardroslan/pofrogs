<?php
require_once ('../../Connections/api.php');
if (!isset($_SESSION)) {
	$session_lifetime = 3600 * 24; // 1 days
	session_set_cookie_params ($session_lifetime);
	session_start();
}
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

//get ip address of user
function getRealIpAddr(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
$ipaddress = getRealIpAddr();

//function to update log table
function update_log($log_action, $log_message){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $logSQL = sprintf("INSERT INTO sys_log (action, message, datetime, logby) VALUES (%s, %s, now(), %s)",
                GetSQLValueString($log_action, "text"),
                GetSQLValueString($log_message, "text"),
                GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
        mysql_select_db($database_api, $api);
        $logResult = mysql_query($logSQL, $api) or die(mysql_error());
        
        if(!$logResult) mysql_query("ROLLBACK");
        else mysql_query("COMMIT");
    }return false;
}

//function to create record into sys_report table
function action_create($title, $description, $no_frog_birth, $no_frog_death, $no_frog_mating, $no_frog_description, $pond_condition, $pond_condition_description, $report_date){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        $entityid = '';
        mysql_query("START TRANSACTION");
        $insertSQL = sprintf("INSERT INTO sys_report(title, description, no_frog_birth, no_frog_death, no_frog_mating, no_frog_description, pond_condition, pond_condition_description, report_date, reportby)
    			VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                GetSQLValueString($title, "text"),
                GetSQLValueString($description, "text"),
                GetSQLValueString($no_frog_birth, "int"),
                GetSQLValueString($no_frog_death, "int"),
                GetSQLValueString($no_frog_mating, "int"),
                GetSQLValueString($no_frog_description, "text"),
                GetSQLValueString($pond_condition, "text"),
                GetSQLValueString($pond_condition_description, "text"),
                GetSQLValueString($report_date, "text"),
                GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($insertSQL, $api) or die(mysql_error());
        $GLOBALS['id'] = mysql_insert_id();
        
        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{ 
            mysql_query("COMMIT");
            update_log("Report: Create", "[".$GLOBALS['ipaddress']."] Report-".$GLOBALS['id']." has been created.");
            return true;
        }
    }return false;
}

//function to update record in sys_report table
function action_update($id, $title, $description, $no_frog_birth, $no_frog_death, $no_frog_mating, $no_frog_description, $pond_condition, $pond_condition_description, $report_date){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $updateSQL = sprintf("UPDATE sys_report SET title = %s, description = %s, no_frog_birth = %s, no_frog_death = %s, no_frog_mating = %s, no_frog_description = %s, pond_condition = %s, pond_condition_description = %s, report_date = %s WHERE id = %s",
                GetSQLValueString($title, "text"),
                GetSQLValueString($description, "text"),
                GetSQLValueString($no_frog_birth, "int"),
                GetSQLValueString($no_frog_death, "int"),
                GetSQLValueString($no_frog_mating, "int"),
                GetSQLValueString($no_frog_description, "text"),
                GetSQLValueString($pond_condition, "text"),
                GetSQLValueString($pond_condition_description, "text"),
                GetSQLValueString($report_date, "text"),
                GetSQLValueString($id, "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($updateSQL, $api) or die(mysql_error());
        
        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{
            mysql_query("COMMIT");
            update_log("Report: Update", "[".$GLOBALS['ipaddress']."] Report-".$id." has been updated.");
            return true;
        }
    }return false;
}

//function to delete record in sys_report table
function action_delete($id){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $deleteSQL = sprintf("DELETE FROM sys_report WHERE id = %s",GetSQLValueString($id, "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($deleteSQL, $api) or die(mysql_error());

        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{
            mysql_query("COMMIT");
            update_log("Report: Remove", "[".$GLOBALS['ipaddress']."] Report-".$id." has been removed.");
            return true;
        }
    }return false;
}

$return = array();
$json_status = "";
$json_message = "";

$action                     = (isset($_POST['action']) ? $_POST['action'] : null);
$id                         = (isset($_POST['reportid']) ? $_POST['reportid'] : null);
$title                      = (isset($_POST['input_title']) ? $_POST['input_title'] : null);
$description                = (isset($_POST['input_description']) ? $_POST['input_description'] : null);
$no_frog_birth              = (isset($_POST['input_no_frog_birth']) ? $_POST['input_no_frog_birth'] : '0');
$no_frog_death              = (isset($_POST['input_no_frog_death']) ? $_POST['input_no_frog_death'] : '0');
$no_frog_mating             = (isset($_POST['input_no_frog_mating']) ? $_POST['input_no_frog_mating'] : '0');
$no_frog_description        = (isset($_POST['input_no_frog_description']) ? $_POST['input_no_frog_description'] : null);
$pond_condition             = (isset($_POST['input_pond_condition']) ? $_POST['input_pond_condition'] : null);
$pond_condition_description = (isset($_POST['input_pond_condition_description']) ? $_POST['input_pond_condition_description'] : null);
$report_date                = (isset($_POST['input_report_date']) ? $_POST['input_report_date'] : null);

if($action != ""){
    if(strtolower($action) == "create") $result = action_create($title, $description, $no_frog_birth, $no_frog_death, $no_frog_mating, $no_frog_description, $pond_condition, $pond_condition_description, $report_date);
    else if(strtolower($action) == "update") $result = action_update($id, $title, $description, $no_frog_birth, $no_frog_death, $no_frog_mating, $no_frog_description, $pond_condition, $pond_condition_description, $report_date);
    else if(strtolower($action) == "delete") $result = action_delete($id);
    
    if($result){ //return if action success
        $json_status = "success";
        $json_message =  "Record has been ".$action."d successfully.";
    }else{ //return if action failed
        $json_status = "fail";
        $json_message = "Record failed to ".$action.".";
    }
    
    $return[] = array(
        'status'    => $json_status,
        'message'   => $json_message,
    );
    echo json_encode($return);
}

mysql_close($api);
?>