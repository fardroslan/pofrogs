<?php
require_once ('../../Connections/api.php');
if (!isset($_SESSION)) {
    $session_lifetime = 3600 * 24; // 1 days
    session_set_cookie_params ($session_lifetime);
    session_start();
}
if (!function_exists("GetSQLValueString")) {
    function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
    {
        if (PHP_VERSION < 6) {
            $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
        }

        $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

        switch ($theType) {
            case "text":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "long":
            case "int":
                $theValue = ($theValue != "") ? intval($theValue) : "NULL";
                break;
            case "double":
                $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
                break;
            case "date":
                $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
                break;
            case "defined":
                $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
                break;
        }
        return $theValue;
    }
}

//get ip address of user
function getRealIpAddr(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
$ipaddress = getRealIpAddr();

//function to update log table
function update_log($log_action, $log_message){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $logSQL = sprintf("INSERT INTO sys_log (action, message, datetime, logby) VALUES (%s, %s, now(), %s)",
                GetSQLValueString($log_action, "text"),
                GetSQLValueString($log_message, "text"),
                GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
        mysql_select_db($database_api, $api);
        $logResult = mysql_query($logSQL, $api) or die(mysql_error());

        if(!$logResult) mysql_query("ROLLBACK");
        else mysql_query("COMMIT");
    }return false;
}

//function to create record into frog_info table
function action_create($name, $description, $tagcode, $height, $weight, $color, $date_birth, $date_death, $gender, $typeid, $groupid){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        $entityid = '';
        mysql_query("START TRANSACTION");
        $insertSQL = sprintf("INSERT INTO frog_info(name, description, tag_code, height, weight, color, date_birth, date_death, gender, typeid, groupid, createdby)
    			VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                GetSQLValueString($name, "text"),
                GetSQLValueString($description, "text"),
                GetSQLValueString($tagcode, "text"),
                GetSQLValueString($height, "text"),
                GetSQLValueString($weight, "text"),
                GetSQLValueString($color, "text"),
                GetSQLValueString($date_birth, "text"),
                GetSQLValueString($date_death, "text"),
                GetSQLValueString($gender, "text"),
                GetSQLValueString($typeid, "text"),
                GetSQLValueString($groupid, "text"),
                GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($insertSQL, $api) or die(mysql_error());
        $GLOBALS['id'] = mysql_insert_id();

        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{
            mysql_query("COMMIT");
            update_log("FrogInfo: Create", "[".$GLOBALS['ipaddress']."] Frog-".$GLOBALS['id']." has been created.");
            return true;
        }
    }return false;
}

//function to update record in frog_info table
function action_update($id, $name, $description, $tagcode, $height, $weight, $color, $date_birth, $date_death, $gender, $typeid, $groupid){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $updateSQL = sprintf("UPDATE frog_info SET name = %s, description = %s, tag_code = %s, height = %s, weight = %s, color = %s, date_birth = %s, date_death = %s, gender = %s, typeid = %s, groupid = %s WHERE id = %s",
                GetSQLValueString($name, "text"),
                GetSQLValueString($description, "text"),
                GetSQLValueString($tagcode, "text"),
                GetSQLValueString($height, "text"),
                GetSQLValueString($weight, "text"),
                GetSQLValueString($color, "text"),
                GetSQLValueString($date_birth, "text"),
                GetSQLValueString($date_death, "text"),
                GetSQLValueString($gender, "text"),
                GetSQLValueString($typeid, "text"),
                GetSQLValueString($groupid, "text"),
                GetSQLValueString($id, "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($updateSQL, $api) or die(mysql_error());

        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{
            mysql_query("COMMIT");
            update_log("FrogInfo: Update", "[".$GLOBALS['ipaddress']."] Frog-".$id." has been updated.");
            return true;
        }
    }return false;
}

//function to delete record in frog_info table
function action_delete($id){
    include '../../Connections/api.php';
    if($_COOKIE['PoF_UserID'] != ""){
        mysql_query("START TRANSACTION");
        $deleteSQL = sprintf("DELETE FROM frog_info WHERE id = %s",GetSQLValueString($id, "int"));
        mysql_select_db($database_api, $api);
        $Result = mysql_query($deleteSQL, $api) or die(mysql_error());

        if(!$Result){
            mysql_query("ROLLBACK");
            return false;
        }else{
            mysql_query("COMMIT");
            update_log("FrogInfo: Remove", "[".$GLOBALS['ipaddress']."] Frog-".$id." has been removed.");
            return true;
        }
    }return false;
}

$return = array();
$json_status = "";
$json_message = "";

$action             = (isset($_POST['action']) ? $_POST['action'] : null);
$id                 = (isset($_POST['reportid']) ? $_POST['reportid'] : null);
$name               = (isset($_POST['input_name']) ? $_POST['input_name'] : null);
$description        = (isset($_POST['input_description']) ? $_POST['input_description'] : null);
$tagcode            = (isset($_POST['input_tag_code']) ? $_POST['input_tag_code'] : null);
$height             = (isset($_POST['input_height']) ? $_POST['input_height'] : null);
$weight             = (isset($_POST['input_weight']) ? $_POST['input_weight'] : null);
$color              = (isset($_POST['input_color']) ? $_POST['input_color'] : null);
$date_birth         = (isset($_POST['input_date_birth']) ? $_POST['input_date_birth'] : null);
$date_death         = (isset($_POST['input_date_death']) ? $_POST['input_date_death'] : null);
$gender             = (isset($_POST['input_gender']) ? $_POST['input_gender'] : null);
$typeid             = (isset($_POST['input_type']) ? $_POST['input_type'] : null);
$groupid            = (isset($_POST['input_group']) ? $_POST['input_group'] : null);

if($action != ""){
    if(strtolower($action) == "create") $result = action_create($name, $description, $tagcode, $height, $weight, $color, $date_birth, $date_death, $gender, $typeid, $groupid);
    else if(strtolower($action) == "update") $result = action_update($id, $name, $description, $tagcode, $height, $weight, $color, $date_birth, $date_death, $gender, $typeid, $groupid);
    else if(strtolower($action) == "delete") $result = action_delete($id);

    if($result){ //return if action success
        $json_status = "success";
        $json_message =  "Record has been ".$action."d successfully.";
    }else{ //return if action failed
        $json_status = "fail";
        $json_message = "Record failed to ".$action.".";
    }

    $return[] = array(
        'status'    => $json_status,
        'message'   => $json_message,
    );
    echo json_encode($return);
}

mysql_close($api);
?>