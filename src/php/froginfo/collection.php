<?php
require_once ('../../Connections/api.php');
if (!isset($_SESSION)) {
    $session_lifetime = 3600 * 24; // 1 days
    session_set_cookie_params ($session_lifetime);
    session_start();
}

//set singleton id:- request to read single record based on id
$singleton = (isset($_GET['id']) ? $_GET['id'] : null);

$data_records = array();
$return = array();

$per_page = 100; //limit page navigation
$pages = '';
$current_page = '';
$totalRows_data_total = '';

if($singleton == ""){  
    //read total of record from frog_info table
    mysql_select_db($database_api, $api);
    $data_total = mysql_query("SELECT a.*, b.displayname, c.id as groupid, c.title as groupname, d.id as typeid, d.title as typename FROM frog_info a LEFT JOIN sys_acc b ON a.createdby = b.id LEFT JOIN frog_group c ON a.groupid = c.id LEFT JOIN frog_type d ON a.typeid = d.id ORDER BY a.sys_status_date DESC", $api) or die(mysql_error());
    $row_data_total = mysql_fetch_assoc($data_total);
    $totalRows_data_total = mysql_num_rows($data_total);

    $pages = ceil($totalRows_data_total/$per_page);

    $current_page = 0;

    if($page != ""){
        $start = ($page)*$per_page;
        $current_page = $page;
    }else{
        $start = ($current_page)*$per_page;
    }
    //read total of record based on number per page from frog_info table
    mysql_select_db($database_api, $api);
    $data = mysql_query("SELECT a.*, b.displayname, c.id as groupid, c.title as groupname, d.id as typeid, d.title as typename FROM frog_info a LEFT JOIN sys_acc b ON a.createdby = b.id LEFT JOIN frog_group c ON a.groupid = c.id LEFT JOIN frog_type d ON a.typeid = d.id ORDER BY a.sys_status_date DESC LIMIT $start, $per_page", $api) or die(mysql_error());
    $row_data = mysql_fetch_assoc($data);
    $totalRows_data = mysql_num_rows($data);
}else{ 
    //read total of record based on number per page from frog_info table **singleton**
    mysql_select_db($database_api, $api);
    $data = mysql_query("SELECT a.*, b.displayname, c.id as groupid, c.title as groupname, d.id as typeid, d.title as typename FROM frog_info a LEFT JOIN sys_acc b ON a.createdby = b.id LEFT JOIN frog_group c ON a.groupid = c.id LEFT JOIN frog_type d ON a.typeid = d.id WHERE a.id = '$singleton'", $api) or die(mysql_error());
    $row_data = mysql_fetch_assoc($data);
    $totalRows_data = mysql_num_rows($data);
}

if($totalRows_data > 0) {
    do {
        $data_records[] = array(
            'id'                => $row_data['id'],
            'name'              => $row_data['name'],
            'description'       => $row_data['description'],
            'height'            => $row_data['height'],
            'weight'            => $row_data['weight'],
            'color'             => $row_data['color'],
            'date_birth'        => $row_data['date_birth'],
            'date_death'        => $row_data['date_death'],
            'gender'            => $row_data['gender'],
            'tag_code'          => $row_data['tag_code'],
            'typeid'            => $row_data['typeid'],
            'type'              => $row_data['typename'],
            'groupid'           => $row_data['groupid'],
            'group'             => $row_data['groupname'],
            'displayname'       => $row_data['displayname'],
            'sys_status'        => $row_data['sys_status'],
            'sys_status_date'   => $row_data['sys_status_date']
        );
         
    } while ($row_data = mysql_fetch_assoc($data));

    $return[] = array(
            'data'         => $data_records,
            'page'         => $pages,
            'currentpage'  => $current_page,
            'totalpage'    => $totalRows_data_total
    );
    echo json_encode($return);
}

mysql_close($api);
?>