<!DOCTYPE html>
	<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="4; URL=index.php">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <title>PoFrogs System</title>
    <link href="css/bootstrap.min.css?v=5" rel="stylesheet">
    <link href="css/enhanced.css" rel="stylesheet">
    <link href="css/jquery.qtip.min.css" rel="stylesheet">
    <link href="css/api.css?v=6" rel="stylesheet"> <link href="css/gemfive.css?v=1" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font_awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <script src="js/1.11.1/jquery.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.popupWindow.js"></script>
	<script src="script/api.js?v=1"></script>
    <script src="js/jquery.tree.js"></script>
    <script src="js/jquery.qtip.min.js"></script>
    <script src="js/imagesloaded.pkg.min.js"></script>
    <link rel="stylesheet" type="text/css" href="cleditor/jquery.cleditor.css?v=1" />
	<script type="text/javascript" src="cleditor/jquery.cleditor.min.js?v=1"></script>
    </head>
  <body>
  	<div id="wrapper">
  	<!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="font-size:large; padding:10px" href="main.php"> <img src="img/logo_main.gif" style="height:30px; display: inline-block;"> &nbsp; <span style="display:inline-block;">PoFrogs</span></a>
            </div>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        
      	<div id="page-wrapper" style="">
            <div class="container-fluid">
            	<div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Fail <small> Log In</small>
                        </h2>
                    </div>
                </div>
                <!-- /.row -->
            
            	<div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  Notice: Login Failed! Click <a href="index.php" class="alert-link">here</a> to try again.
                        </div>
                    </div>
                </div>
                <!-- /.row -->
			</div>
		</div>
		<div class="panel-footer">
	      <div class="container">
	      	<div class="row">
	        	<div class="col-lg-12">
	            </div>
	        </div>
	      </div>
	    </div>
	</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
</body></html>