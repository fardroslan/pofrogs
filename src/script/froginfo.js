jQuery(window).ready(function(){	
	
	//NAVIGATION CONTROL EVENT//
	$('#nav_prev').click(function(){
		var gopage;
		if($(this).attr('data') == 0) gopage = 0;
		else gopage = parseInt($(this).attr('data'))- 1;
		$("tbody#filedata").html('<tr><td colspan="9" style="text-align:left;height:300px; vertical-align:top; padding:40px"><p id="information" style="margin-bottom:0px !important">Loading data, please wait<span>.</span><span>.</span><span>.</span></p><img src="../img/ajax-loader2.gif" width="128" height="15"></td></tr>');
		var formData = $("#filter_form").serialize();
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "../php/froginfo/collection.php?page="+gopage,
			data: formData,
			success: onSuccess_readFile,
			error: onError
		});
	});
	//NAVIGATION CONTROL EVENT//
	$('#nav_next').click(function(){
		$("tbody#filedata").html('<tr><td colspan="9" style="text-align:left;height:300px; vertical-align:top; padding:40px"><p id="information" style="margin-bottom:0px !important">Loading data, please wait<span>.</span><span>.</span><span>.</span></p><img src="../img/ajax-loader2.gif" width="128" height="15"></td></tr>');
		var formData = $("#filter_form").serialize();
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "../php/froginfo/collection.php?page="+$(this).attr('data'),
			data: formData,
			success: onSuccess_readFile,
			error: onError
		});
	});
	//NAVIGATION CONTROL EVENT//
	$("#nav_currentpage").change(function(){
		$("tbody#filedata").html('<tr><td colspan="9" style="text-align:left;height:300px; vertical-align:top; padding:40px"><p id="information" style="margin-bottom:0px !important">Loading data, please wait<span>.</span><span>.</span><span>.</span></p><img src="../img/ajax-loader2.gif" width="128" height="15"></td></tr>');
		var formData = $("#filter_form").serialize();
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "../php/froginfo/collection.php?page="+$(this).val(),
			data: formData,
			success: onSuccess_readFile,
			error: onError
		});
	});
	
	//RIGHT SLIDER PANEL FOR EDITING//
	$('#form_panel').BootSideMenu({side:"right", autoClose:true});
	
	//DATE FIELD PLUGIN DATE COMPONENT//
	$('#input_date_birth, #input_date_death').datepicker({
		format: "yyyy-mm-dd",
		clearBtn: true,
		autoclose: true,
		todayHighlight: true,
		orientation: "auto"
	});
	
	//NEW BUTTON//
	$('#new_btn').click(function(){
		reset_report();
		if($('#form_panel').attr('data-status') == "closed") $('div.toggler ').click();
	});
	
	//REFRESH BUTTON//
	$('#refresh_btn').click(function(){
		refresh_page();
	});
	
	//DELETE BUTTON//
	$('#delete_btn').click(function(){
		$('#action').val('delete');
		bootbox.dialog({
		  title: "Confirmation",
		  message: '<div class="row">  ' +
	        '<div class="col-md-12"> ' +
	        '<form id="statuslog_form" class="form-horizontal"> ' +
	        '<div class="form-group" style="margin-bottom: 5px !important;"> ' +
	        '<span class="col-md-9" for="name">Are you confirm to delete the frog info?</span> ' +
	        '</div> ' +    
	        '</form></div></div>' +
	        '<script> ' +
	        'jQuery(window).ready(function(){ ' +

	        '}); ' +
	        '</script> ',
	        buttons: {
	            success: {
	                label: "Submit",
	                className: "btn-info successbtn",
	                callback: function(){
	              		$("#report_form").submit();
	              	}
	            },
	            cancel: {
	                label: "Cancel",
	                className: "btn-danger",
	                callback: function(){}
	            }
	        }
		});
	});
	
	//RESET BUTTON//
	$('#reset_btn').click(function(){
		reset_report();
	});
	
	//SAVE BUTTON//
	$('#save_btn').click(function(){
		$('#input_title').css('border-color','');
		$('#input_report_date').css('border-color','');
		$('#input_pond_condition').parent().find('button').css('border-color','');
		
		if($('#reportid').val() != "") $('#action').val('update');
		else $('#action').val('create');
		
		if($('#input_title').val() != ""){
			bootbox.dialog({
			  title: "Confirmation",
			  message: '<div class="row">  ' +
		        '<div class="col-md-12"> ' +
		        '<form id="statuslog_form" class="form-horizontal"> ' +
		        '<div class="form-group" style="margin-bottom: 5px !important;"> ' +
		        '<span class="col-md-9" for="name">Are you confirm to submit the form?</span> ' +
		        '</div> ' +    
		        '</form></div></div>' +
		        '<script> ' +
		        'jQuery(window).ready(function(){ ' +
	
		        '}); ' +
		        '</script> ',
		        buttons: {
		            success: {
		                label: "Submit",
		                className: "btn-info successbtn",
		                callback: function(){
		              		$("#report_form").submit();
		              	}
		            },
		            cancel: {
		                label: "Cancel",
		                className: "btn-danger",
		                callback: function(){}
		            }
		        }
			});
		}else{
			if($('#input_title').val() == "") $('#input_title').css('border-color','red');
			if($('#input_report_date').val() == "") $('#input_report_date').css('border-color','red');
			if($('#input_pond_condition').val() == "") $('#input_pond_condition').parent().find('button').css('border-color','red');
			
			bootbox.alert("Report not complete. Please enter all mandatory fields.", function() {});
		}
	});
		
	//TRIGER ALL NUMERIC FIELDS//
	$('.numeric').numeric();
	
	function onSuccess_sys_menus(data, status){
		if(data != "") $('#sys_menus').html(data);
		else $('#sys_menus').html('');
	}
	
	//ON ERROR AJAX CALLED//
	function onError(data, status){}
	
	
	function onSuccess_readSingleFile(data, status){
		if(data != ""){
			var strs = data[0].data;			
			$('#reportid').val(strs[0].id);
			$('#input_name').val(strs[0].name);
			$('#input_description').val(strs[0].description);
			$('#input_date_birth').val(strs[0].date_birth).datepicker('update');
			$('#input_date_death').val(strs[0].date_death).datepicker('update');
			$('#input_height').val(strs[0].height);
			$('#input_weight').val(strs[0].weight);
			$('#input_color').val(strs[0].color);
			$('#input_gender').val(strs[0].gender);
			$('#input_tag_code').val(strs[0].tag_code);
			$('#input_type').val(strs[0].typeid).selectpicker('select');
			$('#input_group').val(strs[0].groupid).selectpicker('select');
			$('.selectpicker').selectpicker('refresh');
		}
	}
	
	//ON SUCCESS AJAX CALLED//
	function onSuccess_readFile(data, status){
		$('#nav_currentpage').text('0');
		$('#nav_totalpage').text('0');
		$('#nav_next').attr('data', '0');
		$('#nav_prev').attr('data', '0');
		$('#record').text('0/0');
		$("tbody#filedata").html('');
		total_loglist = 0;
		
		if(data != ""){		
			var strs = data[0].data;
			var page = data[0].page;
			var currentpage = data[0].currentpage;
			var totalpage = data[0].totalpage;
			var options = "";
			for(var y=1;y<=parseInt(page);y++){
				options = options + '<option value="'+parseInt(y-1)+'">'+y+'</option>';
			}
			$('#nav_currentpage').html(options);
			$('#nav_currentpage').val((parseInt(currentpage)) || '0');
			$('#nav_totalpage').text(parseInt(page) || 0);
			$('#nav_next').attr('data', parseInt((currentpage))+1 || 0);
			$('#nav_prev').attr('data', parseInt(currentpage) || 0);
			
			if((parseInt(currentpage)+1) != page){ //currentpage != totalpages
				$('#nav_next').removeClass('disabled');
				if(currentpage != 0){
					$('#nav_prev').removeClass('disabled');
				}else{
					$('#nav_prev').addClass('disabled');
				}
			}else{
				$('#nav_next').addClass('disabled');
			}
			
			for(var i=0;i<strs.length;i++){
				var filedata = ''+
					'<tr class="info_detail" id="'+strs[i].id+'">'+						                
	                '<td style="text-align:center; min-width:30px; width:5% !important">'+(i+1)+'.</td>'+
	                '<td style="text-align:left; min-width:150px; width:15% !important"><a>'+strs[i].name+'</a></td>'+
	                '<td style="text-align:left; min-width:120px; width:10% !important">'+strs[i].tag_code+'</td>'+
	                '<td style="text-align:left; min-width:150px; width:15% !important">'+strs[i].group+'</td>'+
	                '<td style="text-align:left; min-width:150px; width:15% !important">'+strs[i].type+'</td>'+
	                '<td style="text-align:left; min-width:120px; width:10% !important">'+strs[i].gender+'</td>'+
	                '<td style="text-align:left; min-width:120px; width:10% !important">'+strs[i].color+'</td>'+
	                '<td style="text-align:center; min-width:120px; width:10% !important">'+strs[i].date_birth+'</td>'+
	                '<td style="text-align:left; min-width:120px; width:10% !important">'+strs[i].displayname+'</td>'+
					'</tr>';
				$("tbody#filedata").append(filedata);
				total_loglist = total_loglist + 1;
			}
			if(parseInt($('#nav_currentpage').val())+1 != parseInt($('#nav_totalpage').text())){
				$('#record').text(((parseInt(strs.length)*(parseInt($('#nav_currentpage').val())+1) ) || '0')+"/"+totalpage);
			}else{
				$('#record').text((totalpage || '0')+"/"+totalpage);
			}
			
			$('tr.info_detail').click(function(){
				reset_report();
				$('#delete_btn').removeClass('disabled');
				if($('#form_panel').attr('data-status') == "closed") $('div.toggler ').click();
		
				$.ajax({
					type: "GET",
					dataType: 'json',
					url: "../php/froginfo/collection.php",
					data: "id="+$(this).attr('id'),
					success: onSuccess_readSingleFile,
					error: onError
				});
			});
		}
		$('#log_record').text(total_loglist);
	}
	
	//REFRESH PAGE FUNCTION//
	function refresh_page(){
		$.ajax({
			type: "GET",
			url: "../menus/sys_menus_pages.php",
			success: onSuccess_sys_menus,
			error: onError
		});
		
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "../php/froginfo/collection.php",
			data: "id=",
			success: onSuccess_readFile,
			error: onError
		});
	}
	
	function onSuccess_populatefilter_froggroup(data, status){
		$('#input_group').val('');
		if(data != ""){
			var strs = data[0].data;
			var options = '<option value="">Choose</option>';
			for(var i=0;i<strs.length;i++){
				options = options + '<option value="'+strs[i].id+'">'+strs[i].title+'</option>';
			}
			$('#input_group').html(options);
		}
		$('.selectpicker').selectpicker('refresh');
	}
	
	function onSuccess_populatefilter_frogtype(data, status){
		$('#input_group').val('');
		if(data != ""){
			var strs = data[0].data;
			var options = '<option value="">Choose</option>';
			for(var i=0;i<strs.length;i++){
				options = options + '<option value="'+strs[i].id+'">'+strs[i].title+'</option>';
			}
			$('#input_type').html(options);
		}
		$('.selectpicker').selectpicker('refresh');
	}
	
	function populatefilter(){
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "../php/filterlist/froggroup.php",
			success: onSuccess_populatefilter_froggroup,
			error: onError
		});
		
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "../php/filterlist/frogtype.php",
			success: onSuccess_populatefilter_frogtype,
			error: onError
		});
	}
	
	refresh_page();
	populatefilter();

	//RESET REPORT FUNCTION//
	function reset_report(){
		$('#delete_btn').addClass('disabled');
		$('#reset_btn').removeClass('disabled');
		$('#save_btn').removeClass('disabled');
		
		$('#reportid').val('');
		$('#input_name').val('');
		$('#input_description').val('');
		$('#input_date_birth').val('').datepicker('update');
		$('#input_date_death').val('').datepicker('update');
		$('#input_height').val('');
		$('#input_weight').val('');
		$('#input_color').val('');
		$('#input_gender').val('');
		$('#input_tag_code').val('');
		$('#input_type').val('');
		$('#input_group').val('');
		$('.selectpicker').selectpicker('refresh');

		$('#input_name').css('border-color','');
		$('#input_date_birth').css('border-color','');
		$('#input_type').parent().find('button').css('border-color','');
		$('#input_group').parent().find('button').css('border-color','');
		$('#input_gender').parent().find('button').css('border-color','');
	}
	
	//FORM SUBMIT ON SUCCESS CALLED//
	function onSuccess_action(data, status){
		if(data != ""){
			bootbox.alert(data[0].message, function() {});
			if(data[0].status == "success"){
				refresh_page();
				reset_report();
				$('div.toggler ').click();
			}
		}
	}
	
	//FORM SUBMIT//
	$("form").submit(function (e) {
		e.preventDefault();
		var formId = this.id;  // "this" is a reference to the submitted form
		if(formId == "report_form"){
			var formData = $("#report_form").serialize();
			$("tbody#filedata").html('<tr><td colspan="9" style="text-align:left;height:300px; vertical-align:top; padding:40px"><p id="information" style="margin-bottom:0px !important">Loading data, please wait<span>.</span><span>.</span><span>.</span></p><img src="../img/ajax-loader2.gif" width="128" height="15"></td></tr>');
			$.ajax({
				type: "POST",
				dataType: 'json',
				url: "../php/froginfo/action.php",
				data: formData,
				success: onSuccess_action,
				error: onError
			});
		}
	});
	
	//AJAX CONFIGURE//
	$.ajaxSetup({
		beforeSend:function(){
			$("#loading").show();
		},
		complete:function(){
			$("#loading").hide();
		}
	});
});
