jQuery(window).ready(function(){	
	function onSuccess_sys_menus(data, status){
		if(data != "") $('#sys_menus').html(data);
		else $('#sys_menus').html('');
	}
	
	function onError(data, status){}
	
	$.ajax({
		type: "GET",
		url: "menus/sys_menus.php",
		success: onSuccess_sys_menus,
		error: onError
	});
	
	function onSuccess_statistic_bygroup(data, status){
		if(data != ""){
			data = $.trim(data);
			$('#chartdiv_by_group').html(data);
		}
	}
	
	function statistic_chart_by_group(){
		$.ajax({
			type: "GET",
			url: "php/statistic/collection.php?chart=bygroup",
			success: onSuccess_statistic_bygroup,
			error: onError
		});
	}
	
	function onSuccess_statistic_bytype(data, status){
		if(data != ""){
			data = $.trim(data);
			$('#chartdiv_by_type').html(data);
		}
	}
	
	function statistic_chart_by_type(){
		$.ajax({
			type: "GET",
			url: "php/statistic/collection.php?chart=bytype",
			success: onSuccess_statistic_bytype,
			error: onError
		});
	}
	
	function onSuccess_statistic_bygender(data, status){
		if(data != ""){
			data = $.trim(data);
			$('#chartdiv_by_gender').html(data);
		}
	}
	
	function statistic_chart_by_gender(){
		$.ajax({
			type: "GET",
			url: "php/statistic/collection.php?chart=bygender",
			success: onSuccess_statistic_bygender,
			error: onError
		});
	}
	
	statistic_chart_by_group();
	statistic_chart_by_type();
	statistic_chart_by_gender();
	
	$('.selectpicker').selectpicker('render');
});

