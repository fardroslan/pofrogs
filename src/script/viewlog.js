jQuery(window).ready(function(){
	
	$(window).resize(function() {
	    $('.main_body').css("padding-top", $(".main_header").height());
	}).resize();
	
	var total_loglist = 0;
	var filter = "";
	var sort_date_col = "DESC";
	
	//NAVIGATION CONTROL EVENT//
	$('#nav_prev').click(function(){
		var gopage;
		if($(this).attr('data') == 0) gopage = 0;
		else gopage = parseInt($(this).attr('data'))- 1;
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "php/logs_read.php?page="+gopage,
			data: "filter="+filter+"&sort="+sort_date_col,
			success: onSuccess_logs,
			error: onError
		});
	});
	//NAVIGATION CONTROL EVENT//
	$('#nav_next').click(function(){
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "php/logs_read.php?page="+$(this).attr('data'),
			data: "filter="+filter+"&sort="+sort_date_col,
			success: onSuccess_logs,
			error: onError
		});
	});
	//NAVIGATION CONTROL EVENT//
	$("#nav_currentpage").change(function(){
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "php/logs_read.php?page="+$(this).val(),
			data: "filter="+filter+"&sort="+sort_date_col,
			success: onSuccess_logs,
			error: onError
		});
	});
	
	function onSuccess_logs(data, status){
		$('#nav_currentpage').text('0');
		$('#nav_totalpage').text('0');
		$('#nav_next').attr('data', '0');
		$('#nav_prev').attr('data', '0');
		$('#record').text('0/0');
		$("tbody#logList").html('');
		$("#filter_text").text('');
		total_loglist = 0;
		
		if(data != ""){		
			
			var strs = data[0].data;
			var page = data[0].page;
			var currentpage = data[0].currentpage;
			var totalpage = data[0].totalpage;
			var options = "";
			for(var y=1;y<=parseInt(page);y++){
				options = options + '<option value="'+parseInt(y-1)+'">'+y+'</option>';
			}
			$('#nav_currentpage').html(options);
			$('#nav_currentpage').val((parseInt(currentpage)) || '0');
			$('#nav_totalpage').text(parseInt(page) || 0);
			$('#nav_next').attr('data', parseInt((currentpage))+1 || 0);
			$('#nav_prev').attr('data', parseInt(currentpage) || 0);
			
			if((parseInt(currentpage)+1) != page){ //currentpage != totalpages
				$('#nav_next').removeClass('disabled');
				if(currentpage != 0){
					$('#nav_prev').removeClass('disabled');
				}else{
					$('#nav_prev').addClass('disabled');
				}
			}else{
				$('#nav_next').addClass('disabled');
			}
			
			for(var i=0;i<strs.length;i++){
				var logList = ''+
							'<tr id="'+strs[i].id+'">'+
							'<td style="text-align:left; min-width:30px; width:5% !important">'+(i+1)+'. </td>'+
							'<td style="text-align:left; min-width:150px; width:15% !important">'+strs[i].action+'</td>'+
							'<td style="text-align:left; min-width:300px; width:55% !important">'+strs[i].message+'</td>'+
							'<td style="text-align:left; min-width:150px; width:10% !important">'+strs[i].displayname+'</td>'+
							'<td style="text-align:center; min-width:120px; width:15% !important">'+strs[i].datelastlogin+'</td>'+
							'</tr>';
				$("tbody#logList").append(logList);
				total_loglist = total_loglist + 1;
			}
			if(parseInt($('#nav_currentpage').val())+1 != parseInt($('#nav_totalpage').text())){
				$('#record').text(((parseInt(strs.length)*(parseInt($('#nav_currentpage').val())+1) ) || '0')+"/"+totalpage);
			}else{
				$('#record').text((totalpage || '0')+"/"+totalpage);
			}
		}
		if(filter != ""){
			$("#filter_text").html("- filter by: <i>"+filter+"</i>");
		}
		$('#log_record').text(total_loglist);
	}
	
	function onError(data, status){}
	
	$.ajax({
		type: "GET",
		dataType: 'json',
		url: "php/logs_read.php",
		data: "filter="+filter+"&sort="+sort_date_col,
		success: onSuccess_logs,
		error: onError
	});
	
	function onSuccess_clearlogs(data, status){
		doFilter();
	}
	
	function doFilter(){
		$.ajax({
			type: "GET",
			dataType: 'json',
			url: "php/logs_read.php",
			data: "filter="+filter+"&sort="+sort_date_col,
			success: onSuccess_logs,
			error: onError
		});
	}
	
	$('#refresh_logs').click(function(){
		doFilter();
	});
	
	$('#sort_date_col').click(function(){
		if(sort_date_col == "DESC"){
			$("#sort_date_icon").removeClass('glyphicon-chevron-down');
			$("#sort_date_icon").addClass('glyphicon-chevron-up');
			sort_date_col = "ASC";
			doFilter();
		}else if(sort_date_col == "ASC"){
			$("#sort_date_icon").removeClass('glyphicon-chevron-up');
			$("#sort_date_icon").addClass('glyphicon-chevron-down');
			sort_date_col = "DESC";
			doFilter();
		}
	});
});

