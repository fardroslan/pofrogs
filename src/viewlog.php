<?php require 'php/authentication/authetication.php';?>
<!DOCTYPE html>
	<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <title>PoFrogs System</title>
    <link href="css/bootstrap.min.css?v=5" rel="stylesheet">
    <link href="css/enhanced.css" rel="stylesheet">
    <link href="css/jquery.qtip.min.css" rel="stylesheet">
    <link href="css/bootstrap-switch.min.css" rel="stylesheet">
	<link href="css/bootstrap-datepicker.min.css" rel="stylesheet">
	<link href="css/BootSideMenu.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css?v=6" rel="stylesheet">
    <link href="css/custom.css?v=2" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font_awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <script src="js/1.11.1/jquery.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.numeric.min.js"></script>
	<script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/jquery.popupWindow.js"></script>
	<script src="js/bootstrap-switch.js"></script>
	<script src="js/BootSideMenu.js"></script>
	<script src="js/jquery.dragscroll.js"></script>
	<script type="text/javascript" src="../js/canvasjs.min.js"></script>
    <script type="text/javascript" src="../js/jquery.canvasjs.min.js"></script>
    <script src="js/bootstrap-notify.min.js"></script>
	<script src="script/viewlog.js?v=4"></script>
    <script src="js/jquery.tree.js"></script>
    <script src="js/jquery.qtip.min.js"></script>
    <script src="js/imagesloaded.pkg.min.js"></script>
    <style>
	@-webkit-keyframes opacity{0%{opacity:1}100%{opacity:0}}@-moz-keyframes opacity{0%{opacity:1}100%{opacity:0}}#information span{-webkit-animation-name:opacity;-webkit-animation-duration:1s;-webkit-animation-iteration-count:infinite;-moz-animation-name:opacity;-moz-animation-duration:1s;-moz-animation-iteration-count:infinite}#information span:nth-child(1){-webkit-animation-delay:100ms;-moz-animation-delay:100ms}#information span:nth-child(2){-webkit-animation-delay:300ms;-moz-animation-delay:300ms}#information span:nth-child(3){-webkit-animation-delay:500ms;-moz-animation-delay:500ms}
	
	.pager{margin:0px 0 !important; display:inline-block !important}
	.pager li>a, .pager li>span {
		display: inline-block;
		padding: 0px 10px;
		background-color: #fff;
		border: 1px solid #ddd;
		border-radius: 10px;
	}
	</style>
    </head>
  <body>
  
  <div id="wrapper">
  	
  		<!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="font-size:large; padding:10px" href="main.php"> <img src="img/logo_main.gif" style="height:30px; display: inline-block;"> &nbsp; <span style="display:inline-block;">PoFrogs</span></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <!--li><a href="#">Alert Name <span class="label label-default">Alert Badge</span></a></li>
                        <li class="divider"></li-->
                        <li><a href="#">No message.</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <!--li><a href="#">Alert Name <span class="label label-default">Alert Badge</span></a></li>
                        <li class="divider"></li-->
                        <li><a href="#">No notification.</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php if($_COOKIE['PoF_UserDisplayName'] != "") echo $_COOKIE['PoF_UserDisplayName']; else echo ""; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-fw fa-user"></i> <strike>View Profile</strike></a></li>
					  	<?php if($_COOKIE['PoF_UserGroup'] == "PoF_Admin"){ ?>
					  	<li><a href="#"><i class="fa fa-fw fa-users"></i> <strike>User Management</strike></a></li>
					  	<li><a href="viewlog.php"><i class="fa fa-fw fa-list"></i> View Logs</a></li>
					  	<?php } ?>
					  	<li class="divider"></li>
					 	<li><a id="logoutBtn" style="role="menuitem" tabindex="-1" href="<?php echo $logoutAction ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="main.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li><a id="refresh_logs" href="#"><span class="glyphicon glyphicon-refresh"></span> Refresh</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
		
		<div id="page-wrapper" style="height:100%">
            <div class="container-fluid">
            	<!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Logs <small>List</small>
                        </h2>
                    </div>
                </div>
                <!-- /.row -->
                
                <div class="row">
                    <div class="col-lg-12">
			          <div class="panel-heading"><span style="display:inline; float:right">Total data:- <label id="record">0/0</label></span>
					  <ul class="pager">
						<li id="nav_prev" class="disabled" data=""><a id="nav_prev_btn" href="#"><span aria-hidden="true">&larr;</span> Previous</a></li> <!--label id="nav_currentpage">0</label-->
						<select id="nav_currentpage" class="select input-sm" style="padding:0px !important; margin:0px !important; height:24px !important; line-height:24px !important">
							<option value="0">0</option>
						</select> of <label id="nav_totalpage">0</label>
						<li id="nav_next" class="disabled" data=""><a id="nav_next_btn"  href="#">Next <span aria-hidden="true">&rarr;</span></a></li>
					  </ul>
			          </div>
			            <div class="table-responsive double-scroll" style="overflow-x: scroll; min-height:300px !important">
			              <table id="data_load" class="table table-hover table-condensed table-responsive table-striped table-bordered table-fixed-header">
			                <thead class="header">
			                <tr>
			                <th style="text-align:center; min-width:30px; width:5% !important; background-color:#ECECEC !important">No.</th>
			                <th style="text-align:center; min-width:150px; width:15% !important; background-color:#ECECEC !important">Logs Action</th>
			                <th style="text-align:center; min-width:300px; width:55% !important; background-color:#ECECEC !important">Logs Messages</th>
			                <th style="text-align:center; min-width:150px; width:10% !important; background-color:#ECECEC !important">Logs By</th>
			                <th id="sort_date_col" style="text-align:center; min-width:120px; width:15% !important; background-color:#ECECEC !important; cursor:pointer !important">Datetime <span id="sort_date_icon" class="glyphicon glyphicon-chevron-down"></span></th> 
			                </tr>
			                </thead>
			                <tbody id="logList">            
			                </tbody>
			              </table>
			        	</div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
        </div>
		<div class="panel-footer">
	      <div class="container">
	      	<div class="row">
	        	<div class="col-lg-12">
	            </div>
	        </div>
	      </div>
	    </div>
    	<div id="loading" style="z-index: 9999999; opacity: 0.3; position: fixed; top: 0px; right: 0px; bottom: 0px; left: 0px; display: none; background-color: rgb(0, 0, 0);">
			<ul class="bokeh"><li></li><li></li><li></li></ul>
		</div>
   	</div>

    
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
</body></html>