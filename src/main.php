<?php require 'php/authentication/authetication.php';?>
<!DOCTYPE html>
	<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <title>PoFrogs System</title>
    <link href="css/bootstrap.min.css?v=5" rel="stylesheet">
    <link href="css/enhanced.css" rel="stylesheet">
    <link href="css/jquery.qtip.min.css" rel="stylesheet">
    <link href="css/bootstrap-switch.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="css/bootstrap-select.min.css" rel="stylesheet">
    <link href="css/style.css?v=6" rel="stylesheet">
    <link href="css/custom.css?v=1" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="css/sb-admin.css?v=5" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css?v=5" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font_awesome/css/font-awesome.min.css?v=5" rel="stylesheet" type="text/css">
    
    <script src="js/1.11.1/jquery.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.numeric.min.js"></script>
    <script src="js/autonumeric.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/jquery.popupWindow.js"></script>
    <script src="js/bootstrap-switch.js"></script>
    <script src="js/bootstrap-notify.min.js"></script>
    <script src="script/main.js?v=6"></script>
    <script type="text/javascript" src="js/canvasjs.min.js"></script>
    <script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>
    <script src="js/jquery.tree.js"></script>
    <script src="js/jquery.qtip.min.js"></script>
    <script src="js/imagesloaded.pkg.min.js"></script>
    
    <script src="js/amcharts.js"></script>
	<script src="js/serial.js"></script>
	<script src="js/pie.js"></script>
	<script src="https://www.amcharts.com/lib/3/pie.js"></script>
	<script src="js/light.js"></script>
    
	<script>
		jQuery(window).ready(function(){
			$('[data-toggle="tooltip"]').tooltip(); 
		});
	</script>
	<style>
		a.new_module{text-decoration: line-through !important;}
		.amcharts-chart-div a{ display:none !important;}
		
    	@-webkit-keyframes opacity{0%{opacity:1}100%{opacity:0}}@-moz-keyframes opacity{0%{opacity:1}100%{opacity:0}}#information span{-webkit-animation-name:opacity;-webkit-animation-duration:1s;-webkit-animation-iteration-count:infinite;-moz-animation-name:opacity;-moz-animation-duration:1s;-moz-animation-iteration-count:infinite}#information span:nth-child(1){-webkit-animation-delay:100ms;-moz-animation-delay:100ms}#information span:nth-child(2){-webkit-animation-delay:300ms;-moz-animation-delay:300ms}#information span:nth-child(3){-webkit-animation-delay:500ms;-moz-animation-delay:500ms}
    	
    	.pager{margin:0px 0 !important; display:inline-block !important}
    	.pager li>a, .pager li>span {
    		display: inline-block;
    		padding: 0px 10px;
    		background-color: #fff;
    		border: 1px solid #ddd;
    		border-radius: 10px;
    	}
	</style>
    </head>
  <body>
  <div id="wrapper">
  	
  		<!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="font-size:large; padding:10px" href="main.php"> <img src="img/logo_main.gif" style="height:30px; display: inline-block;"> &nbsp; <span style="display:inline-block;">PoFrogs</span></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li><a href="#">No message.</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li><a href="#">No notification.</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php if($_COOKIE['PoF_UserDisplayName'] != "") echo $_COOKIE['PoF_UserDisplayName']; else echo ""; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="fa fa-fw fa-user"></i> <strike>View Profile</strike></a></li>
					  	<?php if($_COOKIE['PoF_UserGroup'] == "PoF_Admin"){ ?>
					  	<li><a href="#"><i class="fa fa-fw fa-users"></i> <strike>User Management</strike></a></li>
					  	<li><a href="viewlog.php"><i class="fa fa-fw fa-list"></i> View Logs</a></li>
					  	<?php } ?>
					  	<li class="divider"></li>
					 	<li><a id="logoutBtn" style="role="menuitem" tabindex="-1" href="<?php echo $logoutAction ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div id="sys_menus" class="collapse navbar-collapse navbar-ex1-collapse"></div>
            <!-- /.navbar-collapse -->
        </nav>
  
		<div id="page-wrapper" style="height:100%">
            <div class="container-fluid">
            	<!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Dashboard <small>Statistic</small>
                        </h2>
                    </div>
                </div>
                <!-- /.row -->
            	
            	<div class="row">
                    <div class="col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Frog Population By Group</h3>
                            </div>
                            <div class="panel-body">
                                <div id="chartdiv_by_group"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                    	<div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Frog Population By Gender</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div id="chartdiv_by_gender"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                            	<div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Frog Population by Type</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div id="chartdiv_by_type"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                        
                        
                    </div>
                </div>
                <!-- /.row -->
                
        	</div>
        </div>
        <div class="panel-footer">
	      <div class="container">
	      	<div class="row">
	        	<div class="col-lg-12">
	            </div>
	        </div>
	      </div>
	    </div>
	</div>
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
</body></html>