<?php require_once('Connections/api.php'); ?>
<?php
ini_set("session.gc_maxlifetime", "86400");
if (!isset($_SESSION)) {
  //session_start();
  $session_lifetime = 3600 * 24; // 1 days
  session_set_cookie_params ($session_lifetime);
}
$MM_authorizedUsers = "";
$MM_donotCheckaccess = "true";
// *** Restrict Access To Page: Grant or deny access to this page
function isAuthorized($strUsers, $strGroups, $UserName, $UserGroup) { 
  // For security, start by assuming the visitor is NOT authorized. 
  $isValid = False; 

  // When a visitor has logged into this site, the Session variable PoF_Username set equal to their username. 
  // Therefore, we know that a user is NOT logged in if that Session variable is blank. 
  if (!empty($UserName)) { 
    // Besides being logged in, you may restrict access to only certain users based on an ID established when they login. 
    // Parse the strings into arrays. 
    $arrUsers = Explode(",", $strUsers); 
    $arrGroups = Explode(",", $strGroups); 
    if (in_array($UserName, $arrUsers)) { 
      $isValid = true; 
    } 
    // Or, you may restrict access to only certain users based on their username. 
    if (in_array($UserGroup, $arrGroups)) { 
      $isValid = true; 
    } 
    if (($strUsers == "") && true) { 
      $isValid = true; 
    } 
  } 
  return $isValid; 
}

$MM_restrictGoTo = "accessfail.php";
if (!((isset($_COOKIE['PoF_Username'])) && (isAuthorized("",$MM_authorizedUsers, $_COOKIE['PoF_Username'], $_COOKIE['PoF_UserGroup'])))) { 
  $MM_qsChar = "?";
  $MM_referrer = $_SERVER['PHP_SELF'];
  if (strpos($MM_restrictGoTo, "?")) $MM_qsChar = "&";
  if (isset($_SERVER['QUERY_STRING']) && strlen($_SERVER['QUERY_STRING']) > 0) 
  $MM_referrer .= "?" . $_SERVER['QUERY_STRING'];
  $MM_restrictGoTo = $MM_restrictGoTo. $MM_qsChar . "accesscheck=" . urlencode($MM_referrer);
  header("Location: ". $MM_restrictGoTo); 
  exit;
}
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

function getRealIpAddr(){
if (!empty($_SERVER['HTTP_CLIENT_IP'])){
	$ip=$_SERVER['HTTP_CLIENT_IP'];
}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
    $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
  }else{
    $ip=$_SERVER['REMOTE_ADDR'];
  }
  return $ip;
}
$ipaddress = getRealIpAddr();

mysql_query("START TRANSACTION");
$updateSQL = sprintf("UPDATE sys_acc SET sessionid = %s WHERE id = %s",
			   GetSQLValueString($_COOKIE["PHPSESSID"], "text"),
			   GetSQLValueString($_COOKIE['PoF_UserID'], "int"));
mysql_select_db($database_api, $api);
$Result1= mysql_query($updateSQL, $api) or die(mysql_error());
if(!$Result1){
  mysql_query("ROLLBACK");
}else{
  mysql_query("COMMIT");
  $_SESSION["CurrentSession"] = $_COOKIE["PHPSESSID"];
}

mysql_query("START TRANSACTION");
$logSQL = sprintf("INSERT INTO sys_log (action, message, datetime, logby) VALUES (%s, %s, now(), %s)",
				   GetSQLValueString("Login", "text"),
				   GetSQLValueString("[".$ipaddress."] Login Successful.", "text"),
				   GetSQLValueString($_COOKIE['PoF_UserID'], "int"));

mysql_select_db($database_api, $api);
$logResult = mysql_query($logSQL, $api) or die(mysql_error());

if(!$logResult){
	mysql_query("ROLLBACK");
}else{
	mysql_query("COMMIT");
	$LastLoginSQL = sprintf("UPDATE sys_acc SET lastlogin = now() WHERE id = %s",
				   GetSQLValueString($_COOKIE['PoF_UserID'], "int"));

	mysql_select_db($database_api, $api);
	$LastLoginResult = mysql_query($LastLoginSQL, $api) or die(mysql_error());
}
mysql_close($api);
?>
<!DOCTYPE html>
	<html lang="en">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <meta http-equiv="refresh" content="2; URL=main.php">

    <title>PoFrogs System</title>
    <link href="css/bootstrap.min.css?v=5" rel="stylesheet">
    <link href="css/enhanced.css" rel="stylesheet">
    <link href="css/jquery.qtip.min.css" rel="stylesheet">
    <link href="css/api.css?v=6" rel="stylesheet"> <link href="css/gemfive.css?v=1" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="font_awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <script src="js/1.11.1/jquery.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.popupWindow.js"></script>
	<script src="script/api.js?v=1"></script>
    <script src="js/jquery.tree.js"></script>
    <script src="js/jquery.qtip.min.js"></script>
    <script src="js/imagesloaded.pkg.min.js"></script>
    <link rel="stylesheet" type="text/css" href="cleditor/jquery.cleditor.css?v=1" />
	<script type="text/javascript" src="cleditor/jquery.cleditor.min.js?v=1"></script>
    </head>
  <body>
  <div id="wrapper">
  <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="font-size:large; padding:10px" href="main.php"> <img src="img/logo_main.gif" style="height:30px; display: inline-block;"> &nbsp; <span style="display:inline-block;">PoFrogs</span></a>
            </div>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        
      	<div id="page-wrapper" style="">
            <div class="container-fluid">
            	<div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Success <small> Log In</small>
                        </h2>
                    </div>
                </div>
                <!-- /.row -->
            
            	<div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i>  Login Success! Please wait..
                        </div>
                    </div>
                </div>
                <!-- /.row -->
			</div>
		</div>
		<div class="panel-footer">
	      <div class="container">
	      	<div class="row">
	        	<div class="col-lg-12">
	            </div>
	        </div>
	      </div>
	    </div>
	</div>
<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.min.js"></script>
</body></html>