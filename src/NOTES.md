## How to install
- Create database **`pofrogs`** in MySQL
- Restore database from file **`src->database->pofrogs.sql`** in MySQL
- Clone **`https://fardroslan@gitlab.com/fardroslan/pofrogs.git`** as root folder.
- Configure database connection at **`root/Connections/api.php`** 

## Technology
- PHP, MySQL, JQuery, CSS Bootstrap (Mobile Responsive) , amCharts: JavaScript Charts & Maps

### Login username & password (for test)
- username: **`farhad`** or **`wani`**
- password: **`password123`**

### Development 
- start: **`18/03/2017`** end: **`20/03/2017`**
