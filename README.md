# PoFrogs Management System
system which allows users to manage a pond of frogs.

### System requirement
- PHP 5.6
- MySQL 5.6.30
- Nginx Server / MAMP / WAMP / XAMP

## How to install
- Create database **`pofrogs`** in MySQL
- Restore database from file **`src->database->pofrogs.sql`** in MySQL
- Clone **`https://fardroslan@gitlab.com/fardroslan/pofrogs.git`** as root folder.
- Configure database connection at **`root/Connections/api.php`** 
- Login detail: see bellow

## Technology
- PHP, MySQL, JQuery, CSS Bootstrap (Mobile Responsive) , amCharts: JavaScript Charts & Maps

## Live Demo
Click <a href="http://fastprintmalaysia.com/pofrogs" target="_blank">here</a> to view.

Login username & password (for test)
- username: **`farhad`** or **`wani`**
- password: **`password123`**

